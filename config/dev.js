const webpack = require('webpack');
const path = require('path');

const config = {
    entry: './src/index.js',
    output: {
        path: path.resolve(__dirname, 'bundle'),
        filename: 'bundle.js',
        publicPath: 'http://localhost:8080/bundle/'
    },
    devServer: {
        historyApiFallback: true,
        inline: true,
        host: 'localhost',
        port:8080,
        contentBase: path.resolve(__dirname, '../'),
    },
    watchOptions: {
        aggregateTimeout: 300,
        poll: 300
    },
    module: {
        rules: [
            {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                loader: "babel-loader",
                query:
                    {
                        presets: ['es2017', 'react', 'es2017', 'stage-2']
                    }
            },
            {
                test: /\.scss$/,
                exclude: /(node_modules)/,
                use: [{
                    loader: "style-loader"
                }, {
                    loader: "css-loader"
                }, {
                    loader: "sass-loader"
                }]
            },
            {
                test: /\.woff2?$|\.ttf$|\.eot$|\.svg$|\.png|\.jpe?g|\.gif$/,
                loader: 'file-loader?name=/[name].[ext]'
            }
        ]
    },
    plugins: [
        new webpack.SourceMapDevToolPlugin(),
        new webpack.HotModuleReplacementPlugin(),
    ]
};

module.exports = config;
