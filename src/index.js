'use strict';

import React from 'react';
import ReactDOM from 'react-dom';
import './styles/style.scss';
import {Routers} from './routers'
import {BrowserRouter} from 'react-router-dom';
import { applyMiddleware, createStore } from 'redux';
import { connectRouter } from 'connected-react-router';
import { Provider } from 'react-redux';
import reducer from './redux'
import { composeWithDevTools } from 'redux-devtools-extension';
import { createBrowserHistory } from 'history';
import thunk from 'redux-thunk';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import injectTapEventPlugin from 'react-tap-event-plugin';
import * as firebase from 'firebase/app';
import { config } from './helpers/config';
import { userStatus } from "./redux/modules/userInfo";
import { auth } from "./helpers/authenticate";
import "firebase/auth";
import "firebase/database";
import "firebase/firestore";
import "firebase/messaging";
import "firebase/functions";

injectTapEventPlugin();
firebase.initializeApp(config);

const history = createBrowserHistory();
const store = createStore(connectRouter(history)(reducer), composeWithDevTools(applyMiddleware(thunk)));
const authToken = localStorage.getItem('token');

if ( authToken ) {
    store.dispatch(userStatus(true));
    auth.authenticate(true);
} else {
    store.dispatch(userStatus(false));
    auth.signout(true)
}


ReactDOM.render((
  <Provider store={store}>
    <BrowserRouter>
      <MuiThemeProvider>
        <Routers/>
      </MuiThemeProvider>
    </BrowserRouter>
  </Provider>
), document.getElementById('root'));