'use strict';

import { combineReducers } from 'redux';
import { userLogin } from './modules/userInfo';

export default combineReducers({
    userLogin,
});
