import { LOGIN_USER } from '../actionConst';

const initialState = {
    auth: false
};

export const userStatus = (status) => ({
   type: LOGIN_USER,
   payload: status
});

export function userLogin(state = initialState, action) {
    if (action.type === LOGIN_USER) {
        return {
            ...state,
            auth: action.payload
        };
    }
    return state;
};
