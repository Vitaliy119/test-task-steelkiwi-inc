import React from 'react';
import { auth } from "./authenticate";
import { Redirect, Route} from "react-router-dom";


export const PrivateRoute = ({ component: Component, ...rest }) => (
    <Route
        {...rest}
        render={ props =>
            auth.isAuthenticated
                ? <Component {...props} />
                : <Redirect to={{ pathname: "/", state: { from: props.location }}} />
        }
    />
);
