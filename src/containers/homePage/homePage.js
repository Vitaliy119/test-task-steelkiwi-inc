import React from 'react';
import './homePage.scss';

export default class HomePage extends React.PureComponent {

  render() {
    return(
      <div className="home-page">
        <h1>This is home page</h1>
      </div>
    )
  }
}