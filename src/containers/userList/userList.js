import React from 'react';
import { User } from '../../components/users/users';
import { radioLeft } from '../../styles/materialStyles/userStyles';
import { userList } from "../../action/userInfo";
import { UserTable } from "../../components/userTabl/userTable";
import { RadioButton, RadioButtonGroup } from 'material-ui/RadioButton';
import ErrorBoundary from '../../helpers/errorBoundary';
import { DebounceInput } from 'react-debounce-input';
import './userList.scss';

export default  class UserList extends React.Component {
    constructor(props) {
        super(props);
    }

    state = {
        searchValue: '',
        users: [],
        toggle: 'list',
    };

    handleChange = async (e) => {
        this.setState({searchValue: e.target.value});
        try {
            if (e.target.value.length) {
              const result = await userList(e.target.value);
              this.setState({users: result.data.items});
            } else {
                this.setState({users: []});
            }
        } catch (error) {
            console.log(error)
        }
    };

    handleToggle = e => {
      this.setState({toggle: e.target.value})
    };

    render() {
       return (
            <div className="user-list">
                <RadioButtonGroup name="shipSpeed" className="user-list__radio-group" defaultSelected="list">
                   <RadioButton
                       name="list"
                       value="list"
                       label="List"
                       style={radioLeft}
                       onClick={this.handleToggle}
                   />
                   <RadioButton
                       name="table"
                       value="table"
                       label="Table"
                       onClick={this.handleToggle}
                   />
                </RadioButtonGroup>
                <DebounceInput
                    className="user-list--search-input"
                    placeholder="Search user"
                    debounceTimeout={300}
                    onChange={this.handleChange}
                />
                <ErrorBoundary>
                    {
                        this.state.toggle === 'list'
                            ? this.state.users.map( el =>
                                <User
                                    key={el.id}
                                    name={el.login}
                                    linkGit={el.html_url}
                                />
                            )
                            : <UserTable
                                user={this.state.users}
                            />
                    }
                </ErrorBoundary>
            </div>
        )
    }
}
