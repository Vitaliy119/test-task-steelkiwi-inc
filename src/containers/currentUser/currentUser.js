import React from 'react';
import { currentUser } from "../../action/userInfo";
import './currentUser.scss';

export default class CurrentUser extends React.PureComponent{
    constructor(props) {
        super(props);
    }

    state = {
        user: {}
    };

    componentDidMount() {
        (async () => {
            const name = this.props.match.params.id;
            try {
                const result = await currentUser(name);
                this.setState({user: result.data})
            } catch (error) {
                console.log(error)
            }
         })();
     }

    render() {
        return (
            <div className="current-user">
                <img className="current-user--img" src={this.state.user.avatar_url} alt=""/>
                <div className="current-user__description">
                    <p className="current-user__name">
                        <span className="current-user--header-description">User Name: </span>
                        {this.state.user.name}
                    </p>
                    <p><span className="current-user--header-description">User login: </span> {this.state.user.login}</p>
                    <p><span className="current-user--header-description">User location:</span> {this.state.user.location}</p>
                    <p><span className="current-user--header-description">User bio:</span> {this.state.user.bio}</p>
                    <p><span className="current-user--header-description">Profile creation date:</span> {this.state.user.created_at}</p>
                    <a href={this.state.user.html_url} target="_blank">GitHub page link</a>
                </div>
            </div>
        )
    }
}
