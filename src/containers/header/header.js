import  React from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import { NavLink, Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { auth } from "../../helpers/authenticate";
import * as firebase from 'firebase/app';
import { userStatus } from "../../redux/modules/userInfo";
import './header.scss';
import PropTypes from "prop-types";

class Header extends React.Component {
    constructor(props) {
        super(props)
    }

   logIn = () => {
        const provider = new firebase.auth.GithubAuthProvider();
        firebase.auth().signInWithPopup(provider)
            .then((result) => {
                localStorage.setItem('token', result.additionalUserInfo.username);
                this.props.userStatus(true);
                auth.authenticate(true);
            })
            .catch((error) => {
                console.log(error);
            });
    };

    logOut = () => {
      localStorage.removeItem('token');
      auth.signout(true);
      this.props.history.push('/');
      this.props.userStatus(false);
    };

    render() {
        const { login } = this.props;

        return (
            <header className='header'>
                <div>
                    <h1 className="header--logo">SteelKiwi</h1>
                    <NavLink className="header--link" activeClassName="header--active-link" exact to="/">Home</NavLink>
                    <NavLink className="header--link" activeClassName="header--active-link" to="/user">Users</NavLink>
                </div>
                {
                    !login
                    ? <RaisedButton
                         label="Sign in"
                         primary={true}
                         onClick={this.logIn}
                      />
                    : <RaisedButton
                            label="Sign out"
                            primary={true}
                            onClick={this.logOut}
                        />
                }
            </header>
        )
    }
}

Header.propTypes = {
    login: PropTypes.bool
};

const mapStateToProps = ({ userLogin }) => ({
    login: userLogin.auth
});

const mapDispatchToProps = {
    userStatus
};

export default connect(mapStateToProps, mapDispatchToProps)(Header)