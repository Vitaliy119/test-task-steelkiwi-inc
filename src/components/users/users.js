import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import './users.scss'

export function User ({ name, linkGit }) {
    return (
        <div className="user">
            <h3 className="user--header">User login: {name}</h3>

            <Link className="user--link" to={`/user/${name}`}>Details about user</Link>
            <Link to={linkGit} target="_blank">GitHub page</Link>
        </div>
    )
}

User.propTypes = {
    name: PropTypes.string,
    linkGit: PropTypes.string,
};