import React from 'react';
import PropTypes from "prop-types";
import { Link } from 'react-router-dom';
import './userTable.scss';

export function UserTable ({ user }) {
    return(
        <table className="user-table" border="1">
            <thead>
            <tr>
                <td className="user-table--td">User login</td>
                <td className="user-table--td">Details about user</td>
                <td className="user-table--td">GitHub page</td>
            </tr>
        </thead>
            {
                user.map(el => {
                   return <tbody key={el.id}>
                        <tr>
                            <td className="user-table--td">{el.login}</td>
                            <td className="user-table--td">
                                <Link className="user--link" to={`/user/${el.login}`}>Details</Link>
                            </td>
                            <td className="user-table--td">
                                <Link to={el.html_url} target="_blank">GitHub</Link>
                            </td>
                        </tr>
                   </tbody>
                })
            }
        </table>
    )
}

UserTable.propTypes = {
    user: PropTypes.array,
};