import React, { Fragment } from 'react';
import { Switch, Route } from 'react-router-dom';
import Header from './containers/header/header';
import UserList from './containers/userList/userList';
import HomePage from './containers/homePage/homePage';
import CurrentUser from './containers/currentUser/currentUser';
import { PrivateRoute } from './helpers/privateRoute';

export const Routers = () => (
    <Fragment>
        <Switch>
            <Route path="*" component={ Header } />
        </Switch>
        <Route exact path="/" component={HomePage} />
        <PrivateRoute exact path="/user" component={UserList} />
        <PrivateRoute exact path="/user/:id" component={CurrentUser} />
    </Fragment>
);

